#include "input.h"

SDL_Event e;

void check_input(Player* player, Food* food){
    while(SDL_PollEvent(&e) != 0){
        if(e.type == SDL_QUIT) {
            running = false;
        } else if(e.type == SDL_KEYDOWN){
            switch(e.key.keysym.sym){
                case SDLK_UP:
                case SDLK_w:
                if(player->direction != down) new_direction = up;
                break;

                case SDLK_DOWN:
                case SDLK_s:
                if(player->direction != up) new_direction = down;
                break;

                case SDLK_LEFT:
                case SDLK_a:
                if(player->direction != right) new_direction = left;
                break;
                    
                case SDLK_RIGHT:
                case SDLK_d:
                if(player->direction != left) new_direction = right;
                break;

                case SDLK_g:
                if(player) player->grow();
                break;

                case SDLK_f:
                if(food) food->alive = false;
                break;
            }
        }
    }
}