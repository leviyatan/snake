#include <time.h>
#include "food.h"
#include "init.h"

Food::Food(){
    alive = true;

    piece.x = rand()%(SCREEN_WIDTH/TILE);
    piece.y = rand()%(SCREEN_HEIGHT/TILE);
    piece.w = TILE;
    piece.h = TILE;

    piece.x *= TILE;
    piece.y *= TILE;
}

void Food::draw(){
    SDL_SetRenderDrawColor(renderer, 0x00,0xFF,0x00,0x00);
    SDL_RenderFillRect(renderer, &piece);
}

int Food::get_x(){
    return piece.x;
}

int Food::get_y(){
    return piece.y;
}