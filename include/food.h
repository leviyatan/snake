#ifndef FOOD_H
#define FOOD_H

#include <SDL2/SDL.h>

class Food {
private:
    SDL_Rect piece;

public:
    bool alive;
    
    Food();
    void draw();
    int get_x();
    int get_y();
};

#endif